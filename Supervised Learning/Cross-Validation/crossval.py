import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

df = pd.read_csv("purchased.csv")
X = df[["Age", "EstimatedSalary"]]
Y = df['Purchased']

knn = KNeighborsClassifier(n_neighbors = 5)
logreg = LogisticRegression()

# K-fold cross-validation
cvs_knn = cross_val_score(knn, X, Y, cv=10, scoring="accuracy").mean()
cvs_logreg = cross_val_score(logreg, X, Y, cv=10, scoring="accuracy").mean()
print(cvs_knn)
print(cvs_logreg)

