from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

import matplotlib.pyplot as plt

diabetes = datasets.load_diabetes()

# print(diabetes.DESCR)
# print(diabetes.feature_names)

X = diabetes.data
Y = diabetes.target

# print(X.shape)
# print(Y.shape)

# Split data into train and test set
# 80/20 split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2)

# print(X_train.shape)
# print(X_test.shape)
# print(Y_train.shape)
# print(Y_test.shape)

# Build linear regression model
model = linear_model.LinearRegression()
model.fit(X_train, Y_train)

# Predict
Y_pred = model.predict(X_test)

# Check performance
print("Coefficients:", model.coef_)
print("Intercept:", model.intercept_)
print("Mean Squared Error: %.2f" % mean_squared_error(Y_test, Y_pred))
print("Coefficient of determination: %.2f" % r2_score(Y_test, Y_pred))

# Scatter plot
plt.scatter(Y_test, Y_pred, alpha=0.5)
plt.show()

