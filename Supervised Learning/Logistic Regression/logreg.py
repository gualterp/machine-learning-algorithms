import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

df = pd.read_csv('loans.csv')

# print(df.info())

# Check for missing values
# print(df.isnull().sum())

# Some important columns have missing values. Let's fill them.
df['LoanAmount'] = df['LoanAmount'].fillna(df['LoanAmount'].mean())
df['Credit_History'] = df['Credit_History'].fillna(df['Credit_History'].median())
df.dropna(inplace = True)

# Solved
# print(df.isnull().sum())

# Replace non-numerical variable values with numerical ones
df['Loan_Status'] = df['Loan_Status'].map({"Y": 1, "N": 0})
df['Gender'] = df['Gender'].map({"Male": 1, "Female": 0})
df['Married'] =  df['Married'].map({"Yes": 1, "No": 0})
df['Dependents'] =  df['Dependents'].map({"0": 0, "1": 1, "2": 2, "3+": 3})
df['Education'] =  df['Education'].map({"Graduate": 1, "Not Graduate": 0})
df['Self_Employed'] =  df['Self_Employed'].map({"Yes": 1, "No": 0})
df['Property_Area'] =  df['Property_Area'].map({"Urban": 2, "Rural": 0, "Semiurban": 1})

# Final dataframe version
# print(df.head())

# Split data into train and test set
# 70/30 split

# print(df.shape)
X = df.iloc[1:542, 1:12].values
Y = df.iloc[1:542, 12].values

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.3)

# Fit and predict

model = LogisticRegression()
model.fit(X_train, Y_train)

logreg_prediction = model.predict(X_test) 
print("LogReg accuracy = ", metrics.accuracy_score(logreg_prediction, Y_test))

print("Y_Pred", logreg_prediction)
print("Y_Test", Y_test)


