from sklearn.datasets import make_blobs
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
import numpy as np
import matplotlib.pyplot as plt

X, Y = make_blobs(n_samples = 125, centers = 2, cluster_std=0.6, random_state = 0)


# Split data into train and test set
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 20)
plt.scatter(X_train[:, 0], X_train[:, 1], c=Y_train)

svc = SVC(kernel="linear")
svc.fit(X_train, Y_train)

ax = plt.gca()
xlim = ax.get_xlim()
ax.scatter(X_test[:, 0], X_test[:, 1], c=Y_test, marker="s")

w = svc.coef_[0]
a = -w[0] / w[1] #Slope
xx = np.linspace(xlim[0], xlim[1])
yy = a * xx - (svc.intercept_[0] / w[1])

plt.plot(xx, yy)

Y_pred = svc.predict(X_test)

cm = confusion_matrix(Y_test, Y_pred)

print(cm)
