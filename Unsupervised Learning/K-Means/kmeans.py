import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.preprocessing import scale
from sklearn.cluster import KMeans
from sklearn.metrics import classification_report

iris = datasets.load_iris()
X = scale(iris.data)
y = pd.DataFrame(iris.target)
var_names = iris.feature_names
# print(X[0:10,])

# Fit K-Means
# k = 3 since in this dataset there are 3 kinds of iris
km = KMeans(n_clusters=3)
km.fit(X)

iris_df = pd.DataFrame(iris.data)
iris_df.columns = ["Sepal_Length", "Sepal_Width", "Petal_Length", "Petal_Width"]
y.columns = ['Target']

# Check real classification
color_theme = np.array(["darkgray", "lightsalmon", "powderblue"])
clusters = np.choose(km.labels_, [2,0,1]).astype(np.int64)
plt.subplot(1,2,1)
plt.scatter(x = iris_df.Petal_Length, y = iris_df.Petal_Width,
            c = color_theme[iris.target])
plt.title("Real classification")

# Check clusters
plt.subplot(1,2,2)
plt.scatter(x = iris_df.Petal_Length, y = iris_df.Petal_Width,
            c = color_theme[clusters])
plt.title("K-Means classification")

# Evaluate model
print(classification_report(y, clusters))
# High precision and high recall are good indicators